// GLOBAL
function DOM_ID(id) {
  return document.getElementById(id);
}

// BÀI TẬP 1
DOM_ID("tinhBT1").onclick = function () {
  var result = Number(document.getElementById("soNgayLamBT1").value) * 100000;
  DOM_ID("resultBT1").innerText = result.toLocaleString() + " VND";
};

// BÀI TẬP 2
DOM_ID("tinhBT2").onclick = function () {
  var so1 = Number(DOM_ID("so1BT2").value);
  var so2 = Number(DOM_ID("so2BT2").value);
  var so3 = Number(DOM_ID("so3BT2").value);
  var so4 = Number(DOM_ID("so4BT2").value);
  var so5 = Number(DOM_ID("so5BT2").value);
  var result = (so1 + so2 + so3 + so4 + so5) / 5;
  DOM_ID("resultBT2").innerText = result;
};

// BÀI TẬP 3
DOM_ID("tinhBT3").onclick = function () {
  var soTien = Number(DOM_ID("soTienBT3").value);
  var result = soTien * 23500;
  DOM_ID("resultBT3").innerText = result.toLocaleString() + " VND";
};

// BÀI TẬP 4
DOM_ID("tinhBT4").onclick = function () {
  var chieuDai = Number(DOM_ID("chieuDaiBT4").value);
  var chieuRong = Number(DOM_ID("chieuRongBT4").value);
  var dienTich = chieuDai * chieuRong;
  var chuVi = (chieuDai + chieuRong) * 2;
  DOM_ID("resultDienTichBT4").innerText = dienTich;
  DOM_ID("resultChuViBT4").innerText = chuVi;
};

// BÀI TẬP 5
DOM_ID("tinhBT5").onclick = function () {
  var so = Number(DOM_ID("soBT5").value);
  var hangDonVi = so % 10;
  var hangChuc = Math.floor(so / 10);
  if (hangChuc <= 9) {
    var result = hangChuc + hangDonVi;
    DOM_ID("resultBT5").innerText = result;
  } else {
    DOM_ID("resultBT5").innerText = "Vui lòng nhập số có 2 ký số!";
  }
};
